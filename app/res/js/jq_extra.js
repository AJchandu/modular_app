//$.fn.scrollEnd = function(callback, timeout) {          
//  $(this).on("scroll",function(){
//    var $this = $(this);
//    if ($this.data('scrollTimeout')) {
//      clearTimeout($this.data('scrollTimeout'));
//    }
//    $this.data('scrollTimeout', setTimeout(callback,timeout));
//  });
//};
//NOT WORKING!!

$.fn.transitionEnd = function(callback, timeout) {          
  $(this).on("transitionend webkitTransitionEnd oTransitionEnd",function(){
    var $this = $(this);
    if ($this.data('transitionTimeout')) {
      clearTimeout($this.data('transitionTimeout'));
    }
    $this.data('transitionTimeout', setTimeout(callback,timeout));
  });
};

$.fn.afterwards = function (event, callback, timeout) {
    var self = $(this), delay = timeout || 16;
    self.each(function () { 
        var $t = $(this);
        $t.on(event, function(){
            if ($t.data(event+'-timeout')) {
                clearTimeout($t.data(event+'-timeout'));
            }
            $t.data(event + '-timeout', setTimeout(function () { callback.apply($t); },delay));
        })
    });
    return this;
};
var styler = new function () {
    this.auto_arrange = function (parent, child, min_margin) {
        //        console.log("styling for -> parent : " + parent + ", child : " + child);
        //        console.log("width of parent : " +$(parent).prop("clientWidth"));
        //        console.log("width of child : " +$(child).outerWidth());
        min_margin = min_margin || 10;
        if ($(window).height() < 800)
            min_margin = min_margin / 2;

        var container_width = ($(parent).width() < $(parent).prop("clientWidth")) ? $(parent).width() : $(parent).prop("clientWidth");
        var scroll_bar_width = $(parent).innerWidth() - $(parent).prop("clientWidth");
        container_width -= scroll_bar_width;
        //        console.log("width " + $(parent).width());
        //        console.log("Iwidth " + $(parent).innerWidth());
        //        console.log("Cwidth " + $(parent).prop("clientWidth"));
        //        console.log("Scroll_bar_width " + scroll_bar_width);
        var option_width = $(child).outerWidth();
        option_width += (min_margin * 2) + 2;

        var children_possible_in_a_row = Math.trunc(container_width / option_width);
        var total_children = $(child).length;

        //check if no of children less than no of children fitting in a row.
        var children_in_a_row = total_children < children_possible_in_a_row ? total_children : children_possible_in_a_row;
        var space_occupied_by_options = option_width * children_in_a_row;
        var margin_on_each_side = (container_width - space_occupied_by_options) / (2 * children_in_a_row);


        if (margin_on_each_side == "Infinity" || children_in_a_row == 1) {
            //In case of single column.
            margin_on_each_side = "auto";
            $(child).css("float", "none");
        } else {
            margin_on_each_side += min_margin;
        }

        //    console.log("margin : " + margin_on_each_side);
        $(child).css({
            "margin-left": margin_on_each_side,
            "margin-right": margin_on_each_side
        });
    };
};

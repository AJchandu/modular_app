$(dom_id + " .login_container .close_button").on("click", function () {
    $(dom_id + " .login_container").animate({
        "opacity": "0"
    }, 250, function () {
        $(dom_id + " .login_container").css({
            "display": "none"
        });
    });
});


this.show_account_container = function () {
    $(dom_id + " .login_container > *:not(:last-child)").css("display", "none");
    $(dom_id + " .login_container .wrong_credentials_message,"+ dom_id + " .login_container .credentials_exist_message").css("display", "none");
    $(dom_id + " .login_container .sign_in_form").css("display", "block");

    $(dom_id + " .login_container").css({
        "display": "block"
    });
    $(dom_id + " .login_container").animate({
        "opacity": "1"
    }, 250);
};

$(dom_id + " .sign_up_instead_button").on("click", function () {
    $(dom_id + " .sign_in_form").css("display", "none");
    $(dom_id + " .sign_up_form").css("display", "block").trigger("reset");
});

$(dom_id + " .sign_in_instead_button").on("click", function () {
    $(dom_id + " .sign_up_form").css("display", "none");
    $(dom_id + " .sign_in_form").css("display", "block");
});

var obj_this = this;
this.product_skip = 0;
this.load_product_list = function () {
    //    $(dom_id + " .products_container").css("display", "none");
    //    $(dom_id + " .loading").css("display", "block");
    $.ajax({
        beforeSend: function (jqXHR) {
            handy.jqXHR_pool.push(jqXHR);
        },
        complete: function (jqXHR) {
            handy.jqXHR_pool_remove(jqXHR);
        },
        url: modular.variables.server_script_path + "/product_list/product_list.php",
        data: {
            'prod_type_id': obj_this.prod_type_id,
            'prod_skip': obj_this.product_skip
        },
        type: "POST"

    }).done(function (product_data) {
        //        console.log(product_data);
        //        return;
        try {
            product_data = JSON.parse(product_data);

        } catch (e) {
            console.log(product_data);
            return;
        }
        //        console.log(product_data);
        obj_this.product_skip = obj_this.product_skip + product_data.product_list.length;
        append_products(product_data.product_list);
        $(dom_id + " .list_title").html(product_data.product_type);
        $("title").html(product_data.product_type);
    });

}

this.load_search = function () {
    $(dom_id + " .loading").css("display", "block");
    $.ajax({
        beforeSend: function (jqXHR) {
            handy.jqXHR_pool.push(jqXHR);
        },
        complete: function (jqXHR) {
            handy.jqXHR_pool_remove(jqXHR);
        },
        url: modular.variables.server_script_path + "/search/search_results.php",
        data: {
            'Search_string': obj_this.search_phrase,
            'prod_skip': obj_this.product_skip
        },
        type: "POST"
    }).done(function (search_results) {
        try {
            search_results = JSON.parse(search_results);
        } catch (e) {
            console.log(search_results);
            return;
        }
        //        console.log(search_results);
        var already_listed_product_count = $(dom_id + " .single_product").length;
        //        console.log(already_listed_product_count);
        $(dom_id + " .loading").css("display", "none");
        if (search_results.product_list.length == 0 && already_listed_product_count == 0) {
            $(dom_id + " .list_title").html("No results for \"" + obj_this.search_phrase + "\"");
            $("title").html("No results for \"" + obj_this.search_phrase + "\"");
            return;
        }
        obj_this.product_skip = obj_this.product_skip + search_results.product_list.length;
        append_products(search_results.product_list);
        $(dom_id + " .list_title").html("Search results for \"" + obj_this.search_phrase + "\"");
        $("title").html("Results for \"" + obj_this.search_phrase + "\"");
    });
}

$(dom_id + " .view_more_button").on("click", function () {
    $(dom_id + " .view_more_button").css("display", "none");
    $(dom_id + " .view_more_loading").css("display", "inline-block");
    //    return;
    if (modular.current_view.url == "^/Search") {
        obj_this.load_search();
        //        $(dom_id + " .loading").css("display", "none");
    } else {
        obj_this.load_product_list();
    }

});
var product_template = $(dom_id + " .single_product").clone();
$(dom_id + " .products_container").html("");
product_template.css("display", "grid");

function append_products(product_list) {
    //    console.log(product_list.length);
    if (product_list.length == 0) {
        $(dom_id + " .view_more_button").css("display", "none");
        $(dom_id + " .view_more_loading").css("display", "none");
        $(dom_id + " .no_more_results").css("display", "inline-block");
    } else {
        for (var i = 0; i < product_list.length; i++) {
            single_node = product_template.clone();
            single_node.attr("onclick", "modular.goto('/Product/" + product_list[i]['prod_id'] + "')");
            single_node.find(".product_image").attr("src", modular.variables.content_delivery_network + "/product_images/500px/" + product_list[i]['prod_id'] + "_500px.jpg");
            single_node.find(".product_name").html(product_list[i]['Product name']);
            single_node.find(".product_type").html(product_list[i]['Product type']);

            if (product_list[i]['Offer price'] == "" || product_list[i]['Offer price'] == null) {
                single_node.find(".MRP,.off_percentage").css("display", "none");
                single_node.find(".selling_price").html("Rs." + product_list[i]['MRP']);
            } else {
                single_node.find(".selling_price").html("Rs." + product_list[i]['Offer price']);
                single_node.find(".MRP").html("Rs." + product_list[i]['MRP']);
                var offer_percentage = ((product_list[i]['MRP'] - product_list[i]['Offer price']) / product_list[i]['MRP']) * 100;
                offer_percentage = Math.floor(offer_percentage);
                single_node.find(".off_percentage").html(offer_percentage + "% Off");
            }
            $(dom_id + " .products_container").append(single_node);
        }
        styler.auto_arrange(dom_id + " .products_container", dom_id + " .single_product", 50);
        $(dom_id + " .loading").css("display", "none");
        $(dom_id + " .view_more_loading").css("display", "none");
        $(dom_id + " .view_more_button").css("display", "inline-block");
        $(dom_id + " .products_container").css("display", "block");
    }
}

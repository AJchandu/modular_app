var url = modular.url_pointer();
var url_splits = handy.split_filter(url, '/', [""]);
var obj_this = this;

if (modular.current_view.url == "^/ProductList") {
    obj_js.prod_type_id = url_splits[1];
    obj_js.load_product_list();

} else if (modular.current_view.url == "^/Search") {
    if (typeof url_splits[1] == "undefined") {
        ///not really required unless user directly enters null in URL
        obj_js.search_phrase = "";
        obj_js.load_search();
    } else
        obj_js.search_phrase = url_splits[1];
    obj_js.load_search();
}

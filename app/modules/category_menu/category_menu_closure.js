var category_menu_template = $(dom_id + " .single_menu_container").clone();
var menu_list;
var url = modular.url_pointer();
//alert(url);
url = handy.split_filter(url, '/', [""]);
var cat_id = url[1];
if (cat_id == "" || typeof cat_id == "undefined") {
    fetch_options("first");
} else {
    fetch_options(cat_id);
}

function fetch_options(cat_menu_id) {
    $("title").html("");
    $.ajax({
        beforeSend: function (jqXHR) {
            handy.jqXHR_pool.push(jqXHR);
        },
        complete: function (jqXHR) {
            handy.jqXHR_pool_remove(jqXHR);
        },
        url: modular.variables.server_script_path + "/category_menu/fetch_category_menu.php",
        data: {
            'cat_menu_id': cat_menu_id
        },
        type: "GET"
    }).done(function (category_menu_data) {
        try {
            category_menu_data = JSON.parse(category_menu_data);
        } catch (e) {
            console.log(category_menu_data);
            return;
        }
        console.log(category_menu_data);
        $(dom_id + ' .loading').css({
            'display': 'none'
        });
        $(dom_id + " .category_menu_container").css("display", "block");
        $("title").html(category_menu_data.Label); 
        $(dom_id + " .category_title").html(category_menu_data.Label);
        menu_list = category_menu_data.Categories;
        append_categories_menu(category_menu_data.Categories);
        styler.auto_arrange(dom_id + " .category_menu_container", dom_id + " .single_menu_container", 50);

    });
}



function append_categories_menu(menu_data) {
    $(dom_id + " .category_menu_container").html("");
    category_menu_template.css("display", "grid");
    for (var i = 0; i < menu_data.length; i++) {
        var single_node = category_menu_template.clone();
        single_node.attr("id", i).on('click', function () {
            $(dom_id + " .category_menu_container").css("display", "none");
            $(dom_id + ' .loading').css({
                'display': 'block'
            });
            var i = $(this).attr("id");
            console.log(menu_data);
            if ("product_type_id" in menu_data[i]) {
                //                $(dom_id + ' .loading').css({
                //                    'display': 'none'
                //                })
                modular.goto("/ProductList/" + menu_data[i]["product_type_id"]);
                return;
            }
            var url = modular.project_path + "/BrowseCategory/" + menu_data[i]["cat_menu_id"];
            history.pushState("", "", url);
            fetch_options(menu_data[i]["cat_menu_id"]);
            //            console.log(menu_data[i]);
        });

        single_node.find(".image_div").css("background-image", "url('" + modular.variables.content_delivery_network + "/category_menu_images" + menu_data[i]['Category image'] + "')")
        single_node.find(".category_name").html(menu_data[i]['Label']);
        $(dom_id + " .category_menu_container").append(single_node);
    }
}

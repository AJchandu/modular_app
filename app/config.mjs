var config = {
    global_static_scss: [
        "/app/res/scss/remove_defaults.scss",
        "/app/res/scss/buttons.scss",
        "/app/res/scss/imitate.scss",
        "/app/res/scss/positions.scss"
    ],
    global_dynamic_scss: [
        "/app/res/scss/styler.scss",
        "/app/res/scss/time_fitness_theme.scss"
    ], 
    global_css: [
        "/app/res/css/style1.css",
        "/app/res/css/style2.css"
    ],
    global_js: [
        "/app/res/js/jq_extra.js",
        "/app/res/js/styler.js",
        "/app/res/js/fontawesome-free-5.13.0-web/solid.min.js",
        "/app/res/js/fontawesome-free-5.13.0-web/fontawesome.min.js"
    ],
    wrong_url_fallback_url: "/",
    index: {
        "overflow-y": "scroll",
        // "padding-bottom": "50vh"
    },
    main_modules:{
        intro: {
            modules: "app_intro",
            stay_for: 3000,
        },
        loading: {
            modules: "loading",
        },
        url_error: {
            modules: "url_error",
        },
    },
    initial_modules: [
    //    {
    //        module: "login_signup",
    //        state: "stays"
    //    },
        {
            module: "header",
        },
        {
            module: "side_bar",
            state: "stays"
        },
        {
            module: "enquiry",

        },
        // {
        //     module: "footer",
        //     state: "stays"
        // },
        // {
        //     module: "consent",
        // },
    ],

    views : [
        {
            url: "^/$",
            action: [
                {
                    module: "home",

            },
                {
                    module: "how_it_works",
                    where: "append:.how_it_works_block"
            },
                {
                    module: "footer",
                    units: ["html", "scss"]
            }
        ],
            title: "Time Fitness",
            description: "Treadmills | Orbitrac | Spin bikes | Air bikes | Static bikes | Upright bikes | Benches | Home and Gym equipments | Sales and service. Phone number : 9446795555"
    },
        {
            url: "^/Search",
            action: [
              "product_list",
                {
                    module: "footer",
                    after: ".product_list_page_container"
            }
        ]
    },
        {
            url: "^/ProductList",
            action: [
            "product_list",
                {
                    module: "footer",
                    after: ".product_list_page_container"
            }
        ]
    },
        {
            url: "^/Product",
            action: [
            "view_product",
                {
                    module: "footer",
                    after: ".view_product_page_container"
            }
        ]
    },
        {
            url: "^/BrowseCategory",
            action: [
            "category_menu",
                {
                    module: "footer",
                    after: ".category_menu_page_container"
            }
        ]
    },
        {
            url: "^/ContactUs$",
            action: [
            "contact_us",
                {
                    module: "footer",
                    after: ".contact_us_page_container"
            }
        ],
            title: "Contact us",
            description: "Phone number : 9446795555, Kavunkal Building, Thuravoor, Cherthala, Alappuzha - 688532 "
    },
        {
            url: "^/AboutUs$",
            action: [
            "about_us",
                {
                    module: "footer",
                    after: ".about_us_page_container"
            }
        ],
            title: "About us",
            description: "TIME FITNESS in Thuravoor, Alappuzha is one of the leading business in the Fitness equipments and Sports category. Also known for Fitness Equipment Dealers, Treadmill Dealers, Gymnasium Equipment Dealers, Dumbbells dealers in Alappuzha, Ernakulam."
    }
]

};
export {config};
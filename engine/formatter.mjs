import fs from 'fs';
import {execSync} from 'child_process';
import {config} from "../app/config.mjs";
import sass from "sass";

const modules_dir = "../app/modules/";
const modules_processed_dir = "../app/modules_processed/";
var modular_DOMs = {
    index: "",
    url_error: "",
    curtain:"",
    loading: "",
    intro: "",
    seperator: "",
};

var place_holders = {
    dom_id : "",
    project_path : "",
    closure_js : "",
    module_as  : "",
}

for (var key in modular_DOMs) {
    modular_DOMs[key] = "#" + key + "_" + randomizer();
};
console.log(modular_DOMs);
for (var key in place_holders) {
    place_holders[key] = key + "_" + randomizer();
};

var combined_dynamic_scss = "";
for(var i=0;i<config["global_dynamic_scss"].length;i++){
    // var regex = new RegExp('/app', 'g');
    // config["global_dynamic_scss"][i]=config["global_dynamic_scss"][i].replace(regex,'');
    combined_dynamic_scss += "@import \".."+config["global_dynamic_scss"][i]+"\";\n";
    // combined_dynamic_scss += "@import \"../.."+config["global_dynamic_scss"][i]+"\";\n";
    // combined_dynamic_scss += "@import \"/var/www/html/QPBUNDLE_2/qpbundle_website"+config["global_dynamic_scss"][i]+"\";\n";
}

const formatter = {
    scss: function (scss_content) {
        var scss="$project_path : \""+place_holders['project_path']+"\";\n";
        scss += "$dom_id : \""+place_holders['dom_id']+"\";\n";
        scss += combined_dynamic_scss;
        scss += "#"+place_holders['dom_id']+"{\n" + scss_content + "\n}";
        return scss;
    },
    obj_js: function (obj_js_content) {
        var obj_js = "modular.global['"+place_holders['module_as']+"'].obj_js = new function(){\n";
        obj_js += "const dom_id=\"#"+place_holders['dom_id']+"\";\n";
        obj_js += obj_js_content;
        // obj_js.replace(/\n/g, "\n\t", obj_js);
        obj_js += "\n};\n";

        obj_js = "\n<script>\n" + obj_js + "</script>";
        return obj_js;
    },
    closure_js: function (closure_js_content, units) {
        var closure_js = "(function(){\n";
        closure_js += "const dom_id=\"#"+place_holders['dom_id']+"\";\n";
        if (units.includes("obj_js"))
            closure_js += "var obj_js=modular.global['"+place_holders['module_as']+"'].obj_js;\n";
        closure_js += closure_js_content;
        // closure_js.replace("\n", "\n\t");
        closure_js += "\n})();\n";

        closure_js = "<script class='"+place_holders['closure_js']+"'>\n" + closure_js + "</script>";
        return closure_js;
    }
}; 

function preprocess_and_save_module(module){
    var html_path = modules_dir + module['module'] +"/"+ module['module'] + ".html";
    var obj_js_path = modules_dir + module['module'] +"/"+ module['module'] + "_obj.js";
    var closure_path = modules_dir + module['module'] +"/"+ module['module'] + "_closure.js";
    var scss_path = modules_dir + module['module'] +"/"+ module['module'] + ".scss";
    var html="",obj_js="",css="",closure_js="";
    if(fs.existsSync(html_path)){
        html = fs.readFileSync(html_path,"utf-8");
        module['units'].push('html');
    } 
    if(fs.existsSync(obj_js_path)){
        obj_js = fs.readFileSync(obj_js_path,"utf-8");
        obj_js = formatter.obj_js(obj_js);
        module['units'].push('obj_js');
    }
    if(fs.existsSync(closure_path)){
        var closure_js = fs.readFileSync(closure_path,"utf-8");
        closure_js = formatter.closure_js(closure_js, module['units']);
        module['units'].push('closure_js');
    }
    if(fs.existsSync(scss_path)){
        // console.log("scss found");
        var scss = fs.readFileSync(scss_path,"utf-8");
        scss = formatter.scss(scss);
        fs.writeFileSync(modules_processed_dir + module['module'] +"/"+ module['module'] + ".scss",scss,"utf-8");
        module['units'].push('scss');
        var scss_formatted_path = modules_processed_dir + module['module'] +"/"+ module['module'] + ".scss";
        var css_path = modules_processed_dir + module['module'] +"/"+ module['module'] + ".css";
        try{
            // console.log("sass \""+ scss_formatted_path +"\" \""+ css_path +"\"");
            // execSync("sass \""+ scss_formatted_path +"\" \""+ css_path +"\"");
            var result = sass.renderSync({file: scss_formatted_path});
            fs.writeFileSync(css_path,result.css,"utf-8");
        }
        catch(err){
            err.message;
            console.log(err.message);
            return false;
        }
        if(fs.existsSync(css_path)){
            css = "<style>\n" +fs.readFileSync(css_path,"utf-8") +"\n</style>";
        }
    }
    if(html || css || obj_js ){
        var scss = fs.readFileSync(scss_path,"utf-8");
        html = "<div id=\""+place_holders['dom_id']+"\">\n"+css+"\n"+html + obj_js +"\n</div>\n"+closure_js;
        fs.writeFileSync(modules_processed_dir + module['module'] +"/"+ module['module'] + ".html",html,"utf-8"); 
    } 
    return true;
}

function module_data_formatter(module_data,module_type){
    var module_list = [];
    function module_creater(obj){
        var return_obj = {
            module : obj.module,
            container : modular_DOMs[obj.container]??modular_DOMs["index"],
            where : obj.where??(module_type=="view"?"before:"+modular_DOMs["seperator"]:"append:"),
            as : obj.as??obj.module,
            state : obj.state??(module_type=="view"?"leaves":"stays"),
        },merge_obj={};
        switch(module_type){
            case "intro":
                merge_obj = {
                    container : obj.container??modular_DOMs["intro"]
                };
                break;
            case "loading":
                merge_obj = {
                    container : obj.container??modular_DOMs["loading"]
                };
                break;
            case "url_error":
                merge_obj = {
                    container : obj.container??modular_DOMs["url_error"]
                }
                break;
        }
        return return_obj={
            ...return_obj,
            ...merge_obj
        };
    }
    if(typeof module_data == "string"){
        module_list.push(module_creater({
            "module" : module_data
        }));
    }else if(typeof module_data.length == "undefined"){
        module_list.push(module_creater(module_data));
    }else{
        for(var i=0;i<module_data.length;i++){
            var obj;
            if(typeof module_data[i] == "string"){
                obj = module_creater({
                    "module" : module_data[i]
                });
            }else if(typeof module_data[i].length == "undefined"){
                obj = module_creater(module_data[i]);
            }
            module_list.push(obj);
        }
    }
    return module_list;
}

function combine_static_styles(){
    console.log("\n\x1b[1m\x1b[93m%s\x1b[0m","Creating combine_static_styles");
    var combined_static_scss = "", converted_css, combined_css = "";
    for(var i=0;i<config['global_static_scss'].length;i++){
        combined_static_scss+=fs.readFileSync("../"+config['global_static_scss'][i],"utf-8");
    }
    fs.writeFileSync(modules_processed_dir+"combined_static.scss",combined_static_scss,"utf-8");
    try{
        converted_css = execSync("sass \""+ modules_processed_dir+"combined_static.scss\"  ");
    }
    catch(err){
        err.message;
        return false;
    }
    
    for(var i=0;i<config['global_css'].length;i++){
        combined_css += fs.readFileSync("../"+config['global_css'][i],"utf-8");
    }
    fs.writeFileSync(modules_processed_dir +"combined.css", converted_css + combined_css,"utf-8");
}



function combine_js(modules_list){
    console.log("\n\x1b[1m\x1b[93m%s\x1b[0m","Creating combined.js");
    var combined_js = fs.readFileSync("browser_js/jquery-3.3.1.min.js","utf-8");
    combined_js += fs.readFileSync("browser_js/handy.js","utf-8");
    combined_js += fs.readFileSync("browser_js/processes.js","utf-8");
    combined_js += fs.readFileSync("browser_js/kernal.js","utf-8");
    //config = array_merge(config, variables);
    config["modules"] = modules_list;
    combined_js += "\nmodular.modular_DOMs=JSON.parse('"+JSON.stringify(modular_DOMs)+"');";
    combined_js += "\nmodular.place_holders=JSON.parse('"+JSON.stringify(place_holders)+"');";
    combined_js += "\nmodular.config=JSON.parse('"+JSON.stringify(config)+"');";
    combined_js += "\nmodular.views=JSON.parse('"+JSON.stringify(config['views'])+"');";
    combined_js += "\n"+fs.readFileSync("browser_js/boot_up.js","utf-8");
    for(let i=0;i<config['global_js'].length;i++){
        combined_js += fs.readFileSync("../"+config['global_js'][i]);
    }
    fs.writeFileSync(modules_processed_dir +"combined.js",combined_js,"utf-8");
}

function randomizer () {
    var date = new Date();
    var time = date.getTime();
    time = time.toString();
    var math = Math.random();
    math = math.toString();
    var random = time + math;
    random = random.split('.');
    random = random.join('');
    return (random);
};

export {
    modules_dir,
    modules_processed_dir,
    module_data_formatter,
    preprocess_and_save_module,
    combine_static_styles,
    combine_js
};

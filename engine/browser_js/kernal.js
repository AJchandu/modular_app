window.modular = new function () {

    //BEGIN user defined variables
    this.project_path = "";
    this.variables = {};
    this.views = [];
    this.set_theme = {};
    //END** user defined variables

    //BEGIN modular global variables
    this.logs = [];
    this.loaded_modules = [];
    this.current_view = "";
    this.running_process_count = 0;
    this.current_state= "booting";
    //END** modular global variables

    //BEGIN modular global constants
    var obj_this = this;
    this.modular_version = '08.08.18';
    this.place_holders="";
    //END** modular global constants

    //BEGIN modular global user variables
    this.global = {};
    //END** modular global variables

    this.modules_append_waiting_list_pool = [];
    this.fetched_module_data = [];
    this.module_appending_status = "idle";
    this.append_modules = function (modules) {
        obj_this.modules_append_waiting_list_pool = obj_this.modules_append_waiting_list_pool.concat(modules);
        if (obj_this.module_appending_status == "idle") {
            obj_this.module_appending_status = "running";
            this.modules_append_loop();
        }

    };

    this.check_clear_module_name_repetition = function (modules) {
        //BEGIN clearing duplicates in the sent list
        var j = 0;
        for (i = 0; i < modules.length; i++) {
            j = i + 1;
            if (typeof modules[i] != "function")
                while (j < modules.length) {
                    if (typeof modules[j] == "function")
                        j++;
                    else if (modules[i].as == modules[j].as) {
                        obj_this.console_log("Module name repeated - \"" + modules[j].as + "\"", "error");
                        modules.splice(j, 1);
                    } else
                        j++;
                }
        }
        //END clearing duplicates in the sent list

        //BEGIN clear modules in waiting list
        var j = 0;
        for (i = 0; i < modules.length; i++) {
            if (typeof modules[i] != "function")
                while (j < obj_this.modules_append_waiting_list_pool.length) {
                    if (typeof obj_this.modules_append_waiting_list_pool[j] == "function")
                        j++;
                    else if (modules[i].as == obj_this.modules_append_waiting_list_pool[j]) {
                        obj_this.console_log("Module already in waiting list with name - \"" + modules[i].as + "\"", "error");
                        modules.splice(i, 1);
                        j = 0;
                    } else
                        j++;
                }
        }
        //END clear modules in waiting list

        //BEGIN clear already loaded modules
        var j = 0;
        for (i = 0; i < modules.length; i++) {
            if (typeof modules[i] != "function")
                while (j < obj_this.loaded_modules.length) {
                    if (modules[i].as == obj_this.loaded_modules[j]) {
                        obj_this.console_log("Already loaded module with name - \"" + modules[i].as + "\"");
                        modules.splice(i, 1);
                        j = 0;
                    } else
                        j++;
                }
        }
        //END clear already loaded modules
        return modules;
    }

    this.modules_append_loop = function () {
        var popped_module = obj_this.modules_append_waiting_list_pool.shift();
        if (typeof popped_module == "function") {
            popped_module();
            modules_append_loop_end_check();
            return;
        }

        // if (popped_module.after != "") {
        //     if ($(popped_module.after).length)
        //         module_fetch_html(popped_module);
        //     else {
        //         obj_this.console_log("\nCould not find'" + popped_module.after + "'\n appending HTML part of module " + popped_module["module"] + " as " + popped_module["module"] + " if exists failed", "error");
        //         module_fetch_obj_js(popped_module);
        //     }
        // } else
            fetch_module(popped_module);
    };

    function fetch_module(popped_module) {

        var url = obj_this.project_path + "/app/modules_processed/" + popped_module.module + "/" + popped_module.module + ".html?last_modified=" + obj_this.last_modified;

        $.ajax({
            beforeSend: function (jqXHR) {
                handy.jqXHR_pool.push(jqXHR);
            },
            complete: function (jqXHR) {
                handy.jqXHR_pool_remove(jqXHR);
            },
            url: url,
            dataType: "text"
        }).done(function (data) {
            popped_module.dom_id = popped_module.as + "_" + handy.randomizer();
            popped_module.html=data;
            replace_place_holders(popped_module);
        }).fail(function (jqXHR, textStatus) {
            if (textStatus == "abort") {
                obj_this.console_log("\nHTML fetch aborted, \n url = " + url, "alert");
            } else {
                obj_this.console_log("\nCombined HTML fetching failed with URL\n" + url, "error");
            }
        }).always(function (data_or_jqXHR, textStatus) {
            if (textStatus != "abort")
                module_fetch_closure_js(popped_module);
        });
    }
    
    function replace_place_holders(popped_module){
        var regex = new RegExp(obj_this.place_holders["dom_id"], 'g');
        popped_module.html=popped_module.html.replace(regex,popped_module["dom_id"]);
        var regex = new RegExp(obj_this.place_holders["module_as"], 'g');
        popped_module.html=popped_module.html.replace(regex,popped_module["as"]);
        var regex = new RegExp(obj_this.place_holders["project_path"], 'g');
        popped_module.html=popped_module.html.replace(regex,obj_this.project_path);
        popped_module.html=$("<div />").html(popped_module.html);
    }
    function module_fetch_closure_js(popped_module) {
        popped_module.closure_js = popped_module.html.find("script."+obj_this.place_holders.closure_js);
        popped_module.html.find("script."+obj_this.place_holders.closure_js).remove();
        // console.log(popped_module.html.html());
        // console.log(popped_module.closure_js.html());
        modules_append_loop_end_check(popped_module);
    }

    function modules_append_loop_end_check(popped_module) {
        if (typeof popped_module != "undefined") {
            if (("html" in popped_module) || ("closure_js" in popped_module)) {
                append_html_and_callbacks(popped_module);
                obj_this.fetched_module_data.push(popped_module);
            } else {
                obj_this.console_log("\nNo units defined for module : " + popped_module.module, "error");
                obj_this.console_log("Failed adding module as : " + popped_module.as + "\n", "error");
            }
        }
        // return;
        if (obj_this.modules_append_waiting_list_pool.length) {
            obj_this.modules_append_loop();
        } else {
            obj_this.module_appending_status = "idle";
            exec_all_closure_js();
            obj_this.fetched_module_data = [];
            $(obj_this.modular_DOMs.loading).animate({
                "opacity": "0"
            }, function () {
                $(this).css("display", "none");
            })
            console.log("\n\nLoaded modules list for current view\n");
            console.table(obj_this.loaded_modules);
        }
    }
    //
    function append_html_and_callbacks(popped_module) {
        obj_this.global[popped_module["as"]]={};
        obj_this.loaded_modules.push(popped_module);
        // if (popped_module.after != "") {
        //     $(where_DOM+" "+popped_module.after).after(popped_module.html.html());
        // } else {
        //     $(where_DOM).append(popped_module.html.html());
        // }
        var where=popped_module.where.split(':');

        // console.log(where);
        // console.log(where[0]);

        var where_DOM=popped_module.container;
        where[1]?where_DOM+=" "+where[1]:0;

        $(where_DOM)[where[0]](popped_module.html.html());
        obj_this.correct_paths_in_html_module(popped_module.dom_id);

        setTimeout(function () {
            $('#' + popped_module.dom_id).animate({
                opacity: 1
            }, 200);
        }, 100);

        obj_this.console_log("Activated module : \"" + popped_module.module + "\", as : \"" + popped_module.as + "\"");
        delete popped_module.html;
        delete popped_module.props;
    }
    //
    this.correct_paths_in_html_module = function (dom_id) {
        if (obj_this.project_path != "") {
            $("#" + dom_id + " img").each(function () {
                var src = $(this).attr("src");
                if (!src)
                    return;
                src = obj_this.project_path + src;
                $(this).attr("src", src);
            });

            $("#" + dom_id + " a").each(function () {
                var href = $(this).attr("href");
                var regex = new RegExp("^http");
                if (regex.exec(href))
                    return;
                if (!href)
                    return;
                href = obj_this.project_path + href;
                $(this).attr("href", href);
            });
        }
    };

    function exec_all_closure_js() {
        //executing all closure_js
        for (i in obj_this.fetched_module_data) {
            $("#"+obj_this.fetched_module_data[i]["dom_id"]).append(obj_this.fetched_module_data[i].closure_js);
        }
        //END** executing all closure_js
    }

    this.url_to_view_index = function (url_pointer) {
        for (i in obj_this.views) {
            var regex = new RegExp(obj_this.views[i].url);
            if (regex.test(url_pointer)) {

                return i;
            }
        }
        return "url not defined";
    };

    this.goto = function (url, data) {
        var regex = new RegExp(obj_this.current_view.url);
        if (regex.test(url)) {
            obj_this.console_log("Call to the same view ignored, call url = " + url, "alert");
            return;
        }
        var view_index = obj_this.url_to_view_index(url);
        if (view_index == "url not defined") {
            obj_this.wrong_url_notification();
            obj_this.console_log("Call to undefined url via modular.goto(), url = " + url, "alert");
            return;
        }

        $(obj_this.modular_DOMs.loading).css("display", "block").animate({
            "opacity": "1"
        });
        console.clear();
        $(obj_this.modular_DOMs.index).scrollTop(0);
        obj_this.global.view_data = data ? data : "";
        obj_this.kill_processes_and_clear_variables();
        history.pushState("", "", modular.project_path + url);
        obj_this.remove_loaded_modules_with_state_leaves();

        obj_this.append_modules(obj_this.views[view_index].action);
        obj_this.current_view = $.extend({}, obj_this.views[view_index]);
        obj_this.load_new_view();
        obj_this.update_google_analytics();
    };

    this.popstate = function () {
        var view_index = obj_this.url_to_view_index(obj_this.url_pointer());
        if ("popstate" in obj_this.views[view_index] && (obj_this.current_view.url == obj_this.views[view_index].url)) {
            eval(obj_this.views[view_index].popstate);
            return;
        }

        $(obj_this.modular_DOMs.loading).css("display", "block").animate({
            "opacity": "1"
        });
        console.clear();
        $(obj_this.modular_DOMs.index).scrollTop(0);
        obj_this.kill_processes_and_clear_variables();
        obj_this.remove_loaded_modules_with_state_leaves();

        obj_this.append_modules(obj_this.views[view_index].action);
        obj_this.current_view = $.extend({}, obj_this.views[view_index]);
        obj_this.load_new_view();
        obj_this.update_google_analytics();
    };

    this.kill_processes_and_clear_variables = function () {
        handy.jqXHR_abortAll();
        obj_this.modules_append_waiting_list_pool = [];
        obj_this.module_appending_status = "idle";
        obj_this.fetched_module_data = [];
    };

    var unique_removal_class_name_extension = handy.randomizer();
    this.remove_loaded_modules_with_state_leaves = function () {
        for (i = 0; i < obj_this.loaded_modules.length; i++) {
            if (obj_this.loaded_modules[i].state == "leaves") {
                $("#" + obj_this.loaded_modules[i].dom_id + ", #" + obj_this.loaded_modules[i].as + "_script, #" + obj_this.loaded_modules[i].as + "_style").addClass("to_be_removed_" + unique_removal_class_name_extension);
                $(".to_be_removed_" + unique_removal_class_name_extension).animate({
                    opacity: 0
                }, 100, function () {
                    $(".to_be_removed_" + unique_removal_class_name_extension).remove();
                });

                obj_this.loaded_modules.splice(i, 1);
                i--;
            }
        }
        //        console.log("\n\n loaded modules after removing modules with state \"leaves\"");
        //        console.table(handy.clone_obj(obj_this.loaded_modules));
    };
    this.load_new_view = function () {
        if ("title" in obj_this.current_view) {
            document.title = obj_this.current_view.title;
        } else {
            document.title = "";
        }

        if ("description" in obj_this.current_view) {
            $("meta[name=description]").attr({
                "content": obj_this.current_view.description
            });
        } else {
            $("meta[name=description]").attr({
                "content": ""
            });
        }
    };

    this.update_google_analytics = function () {
        if ("google_analytics_ID" in obj_this.variables) {
            console.log("ga push : " + obj_this.url_pointer());
            gtag('config', 'UA-131234488-1', {
                'page_path': obj_this.url_pointer()
            });
            //            gtag('set', 'page', obj_this.url_pointer());
            //            gtag('send', 'pageview');
        }
    };
    this.url_pointer = function (path) {
        path = path || decodeURI(window.location.pathname);
        path = path.replace(obj_this.project_path, "");
        if (path[0] != '/')
            path = '/' + path;
        return path;
    };

    this.wrong_url_notification = function (delay) {
        delay = delay || 0;
        $(obj_this.modular_DOMs.url_error).css({
            "display": "inline",
            "opacity": 0
        }).promise().done(function () {
            $(obj_this.modular_DOMs.url_error).animate({
                opacity: 1
            }, 100);
        });

        setTimeout(function () {
            $(obj_this.modular_DOMs.url_error).fadeOut(function () {
                $(obj_this.modular_DOMs.url_error).css("display", "none");
            });
        }, 3000 + delay);
    };

    this.console_log = function (msg, type) {
        obj_this.logs.push({
            msg: msg,
            type: type
        });
        handy.console_log(msg, type);
    };
};

$(document).on("click", "a", function (event) {
    var e = event ? event : window.event;
    if ($(this).attr("behaviour") == "default" || e.ctrlKey)
        return;

    var a_href = $(this).attr("href");
    var regex = new RegExp("^http");
    if (regex.test(a_href))
        return;

    e.preventDefault();

    if ($(this).attr("behaviour") == "none")
        return;

    var url = modular.url_pointer(a_href);
    modular.goto(url);
});

$(document).ready(function(){

    var script_path = $("script[name='combined']").attr("src");
    
    modular.project_path = script_path.replace("/app/modules_processed/combined.js","");
    modular.last_modified = "";
    //BEGIN create layout

    var common_css = {
        "position": "absolute",
        "overflow": "hidden",
        "width": "100%",
        "height": "100%",
        "left": "0%",
        "top": "0%"
    };

    var index_css = {};
    if ("index" in modular.config)
        index_css = $.extend({}, common_css, modular.config.index);

    $("<div />").appendTo("body").attr({
        "id": modular.modular_DOMs.index.substr(1)
    }).css(index_css).append($("<div />").attr("id",modular.modular_DOMs.seperator.substr(1)).css("display","none"));

    $("<div />").appendTo("body").attr({
        "id": modular.modular_DOMs.url_error.substr(1)
    }).css({
        "opacity": "0"
    }).css(common_css);

    $("<div />").appendTo("body").attr({
        "id": modular.modular_DOMs.loading.substr(1)
    }).css(common_css);

    $("<div />").appendTo("body").attr({
        "id": modular.modular_DOMs.curtain.substr(1)
    }).css({
        "background-color":"white"
    }).css(common_css);

    $("<div />").appendTo("body").attr({
        "id": modular.modular_DOMs.intro.substr(1)
    }).css(common_css);
    //**END create layout
   
    window.onpopstate = function (event) {
        modular.popstate(event);
    };

    //BEGIN appending 'main_modules' and 'initial_modules'
    var modules = [];

    
    modules = modules.concat(modular.config["main_modules"]["intro"]["modules"]);
    modules = modules.concat(modular.config["main_modules"]["loading"]["modules"]);
    modules.push(function(){
        $(modular.modular_DOMs["intro"]+","+modular.modular_DOMs["url_error"]).css("display","none");
        $(modular.modular_DOMs["curtain"]).animate({"opacity":"0"},250,function(){
            $(this).css("display","none");
        });
    });
    modules = modules.concat(modular.config["main_modules"]["url_error"]["modules"]);
    
    if ("initial_modules" in modular.config) {
        modules = modules.concat(modular.config.initial_modules);
    }

    modules = modular.check_clear_module_name_repetition(modules);
    modules.push(load_view);

    // console.log("\n\nInitial modules\n");
    // console.log(modules);
    // console.table(handy.clone_obj(modules));
    // console.log("\n\n");

    modular.append_modules(modules);
    //END** appending 'main_modules' and 'initial_modules'

    function load_view(){
        modular.current_state="loading view";
        var url_pointer = modular.url_pointer();
        var url_to_view_index = modular.url_to_view_index(url_pointer);

        if (url_to_view_index == "url not defined") {
            alert("404");
            return;
            url_to_view_index = modular.url_to_view_index(modular.set_theme.wrong_url_fallback_url);
            if ("wrong_url_fallback_url" in modular.set_theme)
                history.replaceState("", "", modular.project_path + modular.set_theme.wrong_url_fallback_url);
            modules.push(function () {
                var delay = 0;
                if ("intro" in modular.set_theme)
                    if ("stay_for" in modular.set_theme.intro)
                        delay = modular.set_theme.intro.stay_for;
                if (modular.variables.platform != "cordova")
                    modular.wrong_url_notification(delay);
            });
        }
        modular.current_view=modular.config["views"][url_to_view_index];
        var modules=modular.config["views"][url_to_view_index]["action"];
        console.log(modules);
        modular.append_modules(modules);
    }
});
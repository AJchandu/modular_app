//Array.prototype.injectArray = function( idx, arr ) {
//    return this.slice( 0, idx ).concat( arr ).concat( this.slice( idx ) );
//};
Object.defineProperties(Array.prototype, {
    injectArray: {
        enumerable: false,
        writable: false,
        value: function (idx, arr) {
            return this.slice(0, idx).concat(arr).concat(this.slice(idx));
        }
    },withKeyValue:{
        enumerable:false,
        writable:false,
        value:function(key,value){
            for(obj in this){
                if (obj[key]==value)
                return obj
            }
            
        }
    }
});

window.handy = new function () {

    var obj_this = this;

    this.logs = {
        console_log: []
    };

    this.jqXHR_pool = [];
    this.jqXHR_pool_remove = function (jqXHR) {
        var idx = obj_this.find_index_of(obj_this.jqXHR_pool, jqXHR);
        obj_this.jqXHR_pool.splice(idx, 1);
    };

    this.jqXHR_abortAll = function () {
        while (obj_this.jqXHR_pool.length) {
            jqXHR_obj = obj_this.jqXHR_pool.shift();
            jqXHR_obj.abort();
        }
    };

    this.split_filter = function (str, split, filters) {
        filters = filters || "";
        var arr = str.split(split);
        var filtered = new Array();
        for (var i = 0, k = 0, flag; i < arr.length; i++) {
            flag = 0;
            for (var j = 0; j < filters.length; j++) {
                if (arr[i] == filters[j]) {

                    flag = 1;
                    break;
                }
            }
            if (!flag) {
                filtered[k] = arr[i];
                k++;
            }

        }
        return filtered;
    };

    this.find_index_of = function (arr, element) {
        for (i in arr)
            if (arr[i] == element)
                return i;
    };

    this.index_of_obj_with_key_value = function (arr, key, value) {
        for (i in arr) {
            if (key in arr[i])
                if (arr[i].key == value)
                    return i;
        }
    };


    this.console_log = function (msg, type) {
        obj_this.logs.console_log.push({
            msg: msg,
            type: type
        });
        var common = "font-size:15px;";
        if (type == "fatal_error") {
            console.log("%c" + msg, common + "color:red;font-weight:bold;font-size:20px;font-family:'courier new';");
            alert(msg);
        } else if (type == "error")
            console.log("%c" + msg, common + "color:red;font-weight:bold;");
        else if (type == "alert")
            console.log("%c" + msg, common + "color:darkorange;");
        else if (typeof type == 'undefined')
            console.log("%c" + msg, common + "color:darkblue;");
        else if (type == "process")
            console.log("%c" + msg, common + "color:#7f0200;");
    };
    this.apply_line_number_to_ajax_data = function (data) {
        data = data.split("\n");
        var data_with_line_number = "";
        for (i = 0; i < data.length; i++) {
            data_with_line_number += (i + 1) + ". " + data[i] + "\n";
        }
        return data_with_line_number;

    };

    this.imagesLoaded = function () {

        // get all the images (excluding those with no src attribute)
        var $imgs = this.find('img[src!=""]');
        // if there's no images, just return an already resolved promise
        if (!$imgs.length) {
            return $.Deferred().resolve().promise();
        }

        // for each image, add a deferred object to the array which resolves when the image is loaded (or if loading fails)
        var dfds = [];
        $imgs.each(function () {

            var dfd = $.Deferred();
            dfds.push(dfd);
            var img = new Image();
            img.onload = function () {
                dfd.resolve();
            }
            img.onerror = function () {
                dfd.resolve();
            }
            img.src = this.src;

        });

        // return a master promise object which will resolve when all the deferred objects have resolved
        // IE - when all the images are loaded
        return $.when.apply($, dfds);

    };
    this.clone_obj = function (obj) {
        return (JSON.parse(JSON.stringify(obj)));
    };
    this.randomizer = function () {
        var date = new Date();

        time = date.getTime();
        time = time.toString();
        var math = Math.random();
        math = math.toString();


        random = time + math;
        random = random.split('.');
        random = random.join('');
        return (random);
    };

    this.get_form_data = function (form) {
        var form_data = {};
        $(form + " input").each(function () {
            form_data[$(this).prop("name")] = $(this).val();
        });
        $(form + " input[type=radio]:checked").each(function () {
            form_data[$(this).prop("name")] = $(this).val();
        });
        $(form + " select").each(function () {
            form_data[$(this).prop("name")] = $(this).find("option:selected").val();
        });
        $(form + " textarea").each(function () {
            form_data[$(this).prop("name")] = $(this).val();
        });
        return form_data;
    }
    this.timestamp = function (timestamp) {
        this.chumma = function () {
            alert(timestamp);
        };
        this.veruthe = function () {
            console.log(timestamp);
        }
    }
};

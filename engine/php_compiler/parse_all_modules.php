<?php

require "common_global.php";
require "modules_to_obj_format.php";

echo PHP_EOL;
echo "\e[1m\e[93mConverting all module declarations to OBJ format\e[0m" . PHP_EOL;
$config["initial_modules"]=modules_to_obj_format($config["initial_modules"]);
//echo json_encode($config["initial_modules"], JSON_PRETTY_PRINT);
//exit;
for($i=0;$i<count($config["views"]);$i++){
    $config["views"][$i]["action"] = modules_to_obj_format($config["views"][$i]["action"]);
}
//echo json_encode($config["views"], JSON_PRETTY_PRINT);
//echo json_encode($config["initial_modules"], JSON_PRETTY_PRINT) . PHP_EOL;

if(file_exists("../app/modules_processed"))
    system("rm -R ../app/modules_processed");
mkdir("../app/modules_processed");

echo PHP_EOL . "\e[1m\e[93mSubstituting non existent modules\e[0m" . PHP_EOL;
//if(!array_key_exists("intro",$config)){
//    echo "Substituting intro module" . PHP_EOL;
//    $uid = uniqid();
//    $config["intro"]="intro_".$uid;
//    mkdir("../app/modules_processed/intro_".$uid);
//    system("cp substitutes/intro/* ../app/modules_processed/intro_".$uid);
//    rename("../app/modules_processed/intro_".$uid."/intro.html","../app/modules_processed/intro_".$uid."/intro_".$uid.".html");
//    rename("../app/modules_processed/intro_".$uid."/intro.less","../app/modules_processed/intro_".$uid."/intro_".$uid.".less");
//};
if(!array_key_exists("loading",$config)){
    echo "Substituting loading module" . PHP_EOL;
    $uid = uniqid();
    $config["loading"]="loading_".$uid;
    mkdir("../app/modules_processed/loading_".$uid);
    system("cp substitutes/loading/* ../app/modules_processed/loading_".$uid);
    rename("../app/modules_processed/loading_".$uid."/loading.html","../app/modules_processed/loading_".$uid."/loading_".$uid.".html");
    rename("../app/modules_processed/loading_".$uid."/loading.less","../app/modules_processed/loading_".$uid."/loading_".$uid.".less");
};
if(!array_key_exists("url_error",$config)){
    echo "Substituting url_error module" . PHP_EOL;
    $uid = uniqid();
    $config["url_error"]="url_error_".$uid;
    mkdir("../app/modules_processed/url_error_".$uid);
    system("cp substitutes/url_error/* ../app/modules_processed/url_error_".$uid);
    rename("../app/modules_processed/url_error_".$uid."/url_error.html","../app/modules_processed/url_error_".$uid."/url_error_".$uid.".html");
    rename("../app/modules_processed/url_error_".$uid."/url_error.less","../app/modules_processed/url_error_".$uid."/url_error_".$uid.".less");
};
//echo array_key_exists("url_error",$config) . PHP_EOL;
//echo array_key_exists("intro",$config) . PHP_EOL;
//exit;

echo PHP_EOL . "\e[1m\e[93mProcessing all modules\e[0m";
$modules_list_obj=[];
foreach ($modules_list as $key => $value) {
	$modules_list[$key] = ["module" => $value, "units" => []];
    
    echo "\n\e[1m" . $value . "\e[0m\n";
    
	if (!file_exists($modules_processed_dir . $value)) {
		mkdir($modules_processed_dir . $value);
	}
    
    compile_combined_html($modules_list[$key]);
    compile_closure_js($modules_list[$key]);
    $modules_list_obj[$value]=$modules_list[$key]["units"];
}

echo PHP_EOL . "\e[1m\e[93mCreating combined.js\e[0m" . PHP_EOL;
$combined_js = file_get_contents("JS/jquery-3.3.1.min.js");
$combined_js .= file_get_contents("JS/handy.js");
$combined_js .= file_get_contents("JS/processes.js");
$combined_js .= file_get_contents("JS/kernal.js");
//$config = array_merge($config, $variables);
$config = array_merge($config, ["modules" => $modules_list_obj]);
$combined_js .= PHP_EOL . "modular.config=JSON.parse('".json_encode($config)."');";
$combined_js .= PHP_EOL . "modular.views=JSON.parse('".json_encode($config['views'])."');";
$combined_js .= PHP_EOL . file_get_contents("JS/boot_up.js");
file_put_contents("../app/combined.js",$combined_js);
//echo json_encode($config, JSON_PRETTY_PRINT);
?>

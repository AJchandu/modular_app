<?php
function modules_to_obj_format($unformatted_module_data) {
    $to_fullOBJ_format = function($obj){
        return [
          "module" => $obj["module"],  
          "as" => $obj["as"]??$obj["module"],
          "in" => $obj["in"]??"modular.containers.index",
          "after" => $obj["after"]??"",
        ];
    };
	$module_arr = [];
	if (gettype($unformatted_module_data) == "string") {
		return $to_fullOBJ_format([
            "module" => $unformatted_module_data
        ]);
	} else if (array_key_exists("module", $unformatted_module_data)) {
		array_push($module_arr, $unformatted_module_data);
	} else {
		foreach ($unformatted_module_data as $key => $value) {
			if (gettype($value) == "string") {
				array_push($module_arr, $to_fullOBJ_format([
                    "module" => $value
                ]));
			} else {
				array_push($module_arr, $to_fullOBJ_format($value));
			}
		}
	}
	return $module_arr;
}
?>

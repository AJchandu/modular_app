<?php
require "vendor/autoload.php";
use Seld\JsonLint\JsonParser;
function json_file_parse($file_path){
    $json_string = file_get_contents($file_path);
    $json_parsed=json_decode($json_string,true);
    if($json_parsed==null){
        echo "\n\e[31m\e[1mError in JSON file '" . $file_path . "'\e[0m\n";
        $parser = new JsonParser();
        $report = $parser->lint($json_string);
        echo "\e[33m" . $report->getMessage() . "\n\e[0m";
        exit;
    };
    return $json_parsed;
}
?>

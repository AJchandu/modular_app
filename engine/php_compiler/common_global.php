<?php
require "../app/config.php";

$modules_dir="../app/modules/";
$modules_processed_dir="../app/modules_processed/";

clearstatcache();
$modules_list = scandir($modules_dir);
if (($key = array_search(".", $modules_list)) !== false) {
	array_splice($modules_list, $key, 1);
}
if (($key = array_search("..", $modules_list)) !== false) {
	array_splice($modules_list, $key, 1);
}
$combined_dynamic_less_import = "";
foreach ($config["global_dynamic_less"] as $key => $value) {
	$combined_dynamic_less_import = $combined_dynamic_less_import . "\n" . "@import url(\".." . $value . "\");" . PHP_EOL;
}

class formater{
    public function less($module_name){
//        global $modules_dir, $modules_processed_dir, $combined_dynamic_less_import;
        $less = file_get_contents($GLOBALS['modules_dir'] . $module_name . "/" . $module_name . ".less");
        $less = "#__dummy_dom_id__{". PHP_EOL . $less . PHP_EOL . "}";
        $less = "@project_path:\"dummy_project_path\";" . PHP_EOL . $GLOBALS['combined_dynamic_less_import'] . PHP_EOL . $less;
        return $less;
	}
    public function html($module_name){
        return file_get_contents($GLOBALS['modules_dir'] . $module_name . "/" . $module_name . ".html");
	}
    public function obj_js($module_name){
        $obj_js = "modular.global.{module_name_as}.obj_js = new function(){" . PHP_EOL;
        $obj_js .= "const dom_id=\"#__dummy_dom_id__\";" . PHP_EOL;
        $obj_js .= file_get_contents($GLOBALS['modules_dir'] . $module_name . "/" . $module_name . "_obj.js");
        $obj_js = str_replace(PHP_EOL,PHP_EOL."\t",$obj_js);
        $obj_js .= PHP_EOL . "};" . PHP_EOL;
        
        $obj_js = "<script>" . PHP_EOL . $obj_js . "</script>" . PHP_EOL;
        return $obj_js;
	}
    public function closure_js($module_name,$units){
        $closure_js = "(function(){" . PHP_EOL;
        $closure_js .= "const dom_id=\"#__dummy_dom_id__\";" . PHP_EOL;
        if (in_array("obj_js", $units))
            $closure_js .= "var obj_js=modular.global.module_name_as.obj_js;" . PHP_EOL;
        $closure_js .= file_get_contents($GLOBALS['modules_dir'] . $module_name . "/" . $module_name . "_closure.js");
        $closure_js = str_replace(PHP_EOL,PHP_EOL."\t",$closure_js);
        $closure_js .= "})();" . PHP_EOL;

        $closure_js = "<script>" . PHP_EOL . $closure_js . "</script>" . PHP_EOL;
        return $closure_js;
	}
}
$formater = new formater;

function compile_combined_html(&$module_obj){
    $combined_html="<div id='__dummy_dom_id__'>" . PHP_EOL;

    //BEGIN LESS formating
	if (file_exists($GLOBALS['modules_dir'] . $module_obj['module'] . "/" . $module_obj['module'] . ".less")) {
        array_push($module_obj["units"], "less");

        echo "less found\n";
        $less=$GLOBALS['formater']->less($module_obj['module']);
        
        file_put_contents($GLOBALS['modules_processed_dir'] . $module_obj['module'] . "/" . $module_obj['module'] . ".less", $less);
        system("lessc ". $GLOBALS['modules_processed_dir'] . $module_obj['module'] . "/" . $module_obj['module'] . ".less " . $GLOBALS['modules_processed_dir'] . $module_obj['module'] . "/" . $module_obj['module'] . ".css");
	}
    //**END LESS formating

    //BEGIN HTML formating
	if (file_exists($GLOBALS['modules_dir'] . $module_obj['module'] . "/" . $module_obj['module'] . ".html")) {
        array_push($module_obj["units"], "html");
        echo "html found\n";

        $css_processed = file_get_contents($GLOBALS['modules_processed_dir'] . $module_obj['module'] . "/" . $module_obj['module'] . ".css");
        $combined_html .= "<style>" . PHP_EOL . $css_processed . PHP_EOL . "</style>" . PHP_EOL;
        $html = $GLOBALS['formater']->html($module_obj['module']);
        $combined_html .= $html.PHP_EOL;
    }
    //**END HTML formating
    
    //BEGIN obj_js formating
	if (file_exists($GLOBALS['modules_dir'] . $module_obj['module'] . "/" . $module_obj['module'] . "_obj.js")) {
        array_push($module_obj["units"], "obj_js");
        echo "obj.js found\n";

        $obj_js=$GLOBALS['formater']->obj_js($module_obj['module']);
        $combined_html = $combined_html.$obj_js;
    }
    //**END obj_js formating
    
    $combined_html .= "</div>";
    file_put_contents($GLOBALS['modules_processed_dir'] . $module_obj['module'] . "/" . $module_obj['module'] . "_combined.html", $combined_html);
}
function compile_closure_js(&$module_obj){
    //BEGIN closure_js formating
	if (file_exists($GLOBALS['modules_dir'] . $module_obj['module'] . "/" . $module_obj['module'] . "_closure.js")) {
        array_push($module_obj["units"], "closure_js");
        echo "closure.js found\n"; 
        
        $closure_js = $GLOBALS['formater']->closure_js($module_obj['module'],$module_obj["units"]);
        file_put_contents($GLOBALS['modules_processed_dir'] . $module_obj['module'] . "/" . $module_obj['module'] . "_closure.js", $closure_js);
    }
    //**END closure_js formating
}
?>
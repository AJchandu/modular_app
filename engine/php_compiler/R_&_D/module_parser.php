<?php
$initial_module1 = [
	"module",
	[
		"module" => "side_bar",
		"state" => "stays",
	],
	[
		"module" => "enquiry",
		"state" => "stays",

	],
	"privacy",
	[
		"module" => "header",
		"state" => "stays",
	],
];

$initial_module2 = "chumma_oru_module";

$initial_module3 = [
	"module" => "chumma_module",
	"as" => "veruthe_oru_module",
];

function modules_format_parser($unformatted_module_data) {
	$module_arr = [];
	if (gettype($unformatted_module_data) == "string") {
		array_push($module_arr, ["module" => $unformatted_module_data]);
	} else if (array_key_exists("module", $unformatted_module_data)) {
		array_push($module_arr, $unformatted_module_data);
	} else {
		foreach ($unformatted_module_data as $key => $value) {
			if (gettype($value) == "string") {
				array_push($module_arr, ["module" => $value]);
			} else {
				array_push($module_arr, $value);
			}
		}
	}
	return $module_arr;
};

echo json_encode(modules_format_parser($initial_module1), JSON_PRETTY_PRINT);

// var_dump(modules_format_parser($initial_module1));
// var_dump(modules_format_parser($initial_module2));
// var_dump(modules_format_parser($initial_module3));
?>
(function () {

    $(document).on("click", "a", function (event) {
        var e = event ? event : window.event;
        if ($(this).attr("behaviour") == "default" || e.ctrlKey)
            return;

        var a_href = $(this).attr("href");
        var regex = new RegExp("^http");
        if (regex.test(a_href))
            return;

        e.preventDefault();

        if ($(this).attr("behaviour") == "none")
            return;

        var url = modular.url_pointer(a_href);
        modular.goto(url);
    });
    //BEGIN Google analytics
    if ("google_analytics_ID" in modular.variables) {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.setAttribute("async", true);
        script.src = "https://www.googletagmanager.com/gtag/js?id=" + modular.variables.google_analytics_ID;
        document.head.appendChild(script);

        script.onload = function () {
            window.dataLayer = window.dataLayer || [];
            window.gtag = function () {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());
            gtag('config', modular.variables.google_analytics_ID);
            console.log("Google analytics initialized");
        }
    }
    //**END Google analytics

    //BEGIN Google adsense
    if ("google_adsense_ID" in modular.variables) {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.setAttribute("async", true);
        script.src = "https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js";
        document.head.appendChild(script);

        script.onload = function () {
            (adsbygoogle = window.adsbygoogle || []).push({
                google_ad_client: modular.variables.google_adsense_ID,
                enable_page_level_ads: true
            });
            console.log("Google adsense initialized");
        }
    }
    //**END Google adsense

    //BEGIN create layout
    for (key in modular.containers) {
        modular.containers[key] = "#" + key + "_" + handy.randomizer();
    }

    var common_css = {
        "position": "absolute",
        "overflow": "hidden",
        "width": "100%",
        "height": "100%",
        "left": "0%",
        "top": "0%"
    };

    var index_css = {};
    if ("index" in modular.set_theme)
        index_css = $.extend({}, common_css, modular.set_theme.index);

    $("<div />").appendTo("body").attr({
        "id": modular.containers.index.substr(1)
    }).css(index_css);

    $("<div />").appendTo("body").attr({
        "id": modular.containers.url_error.substr(1)
    }).css({
        "opacity": "0"
    }).css(common_css);

    $("<div />").appendTo("body").attr({
        "id": modular.containers.loading.substr(1)
    }).css({
        "background-color": "white",
        "transition": "background-color 2s"
    }).css(common_css);

    $("<div />").appendTo("body").attr({
        "id": modular.containers.intro.substr(1)
    }).css({
        "background": "white"
    }).css(common_css);
    //**END create layout

    window.onpopstate = function (event) {
        modular.popstate(event);
    };

    //BEGIN formating modules entered in variables.js
    var modules = [];

    for (i in modular.views)
        modular.views[i].action = modular.modules_format_parser(modular.views[i].action);

    if ("intro" in modular.set_theme) {
        modular.set_theme.intro = modular.modules_format_parser(modular.set_theme.intro, "intro");
        console.log(modular.set_theme.intro);
        modules = modules.concat(modular.set_theme.intro);
    }

    var default_url_error_loading_common_css = {
        "position": "absolute",
        "left": "50%",
        "top": "50%",
        "-webkit-transform": "translate3d(-50%, -50%, 0)",
        "-moz-transform": "translate3d(-50%, -50%, 0)",
        "-ms-transform": "translate3d(-50%, -50%, 0)",
        "transform": "translate3d(-50%, -50%, 0)",
        "background-color": "white",
        "box-shadow": "0px 0px 10px black",
        "padding": "50px",
        "background-color": "white",
        "box-shadow": "0px 0px 10px black",
        "padding": "50px"
    };
    if ("loading" in modular.set_theme) {
        modular.set_theme.loading = modular.modules_format_parser(modular.set_theme.loading, "loading");
        modular.set_theme.loading[0].state = "stays";
        modules = modules.concat(modular.set_theme.loading);
    } else {
        $("<div>Loading...</div>").appendTo(modular.containers.loading).css(default_url_error_loading_common_css);
    }
    modules.push(function () {
        processes.add("Big bang");
        processes.add("intro fade");

        var intro_stay_delay = 0;
        if ("intro" in modular.set_theme)
            if ("stay_for" in modular.set_theme.intro[0])
                intro_stay_delay = modular.set_theme.intro[0].stay_for;

        setTimeout(fade_intro, intro_stay_delay);

        function fade_intro() {
            $(modular.containers.intro).animate({
                "opacity": "0"
            }, function () {
                $(modular.containers.intro).css({
                    "display": "none"
                });
            });
            processes.minus("intro fade");
        }
    });

    if ("url_error" in modular.set_theme) {
        modular.set_theme.url_error = modular.modules_format_parser(modular.set_theme.url_error, "url_error");
        modular.set_theme.url_error[0].state = "stays";
        modules = modules.concat(modular.set_theme.url_error);
    } else {
        $("<div><b>Error 404.<br>Url does not exist.</b></div> ").appendTo(modular.containers.url_error).css(default_url_error_loading_common_css);
    }
    modules.push(function () {
        $(modular.containers["url_error"]).css({
            "display": "none"
        });
    });

    var url_pointer = modular.url_pointer();
    var url_to_view_index = modular.url_to_view_index(url_pointer);

    if (url_to_view_index == "url not defined") {
        url_to_view_index = modular.url_to_view_index(modular.set_theme.wrong_url_fallback_url);
        if ("wrong_url_fallback_url" in modular.set_theme)
            history.replaceState("", "", modular.project_path + modular.set_theme.wrong_url_fallback_url);
        modules.push(function () {
            var delay = 0;
            if ("intro" in modular.set_theme)
                if ("stay_for" in modular.set_theme.intro)
                    delay = modular.set_theme.intro.stay_for;
            if (modular.variables.platform != "cordova")
                modular.wrong_url_notification(delay);
        });
    }

    if ("initial_modules" in modular.set_theme) {
        modular.set_theme.initial_modules = modular.modules_format_parser(modular.set_theme.initial_modules);
        modules = modules.concat(modular.set_theme.initial_modules);
    }
    //**END formating modules entered in variables.js

    modules = modules.concat(modular.views[url_to_view_index].action);
    //
    console.log("\n\nInitial modules\n");
    console.log(modules);
    console.table(handy.clone_obj(modules));
    console.log("\n\n");

    modules = modular.check_clear_module_name_repetition(modules);


    modules.push(function () {
        $(modular.containers.loading).css({
            "backgroundColor": "transparent",
            "width":"auto",
            "height":"auto",
            overflow:"visible"
        });
        processes.minus("Big bang");
    });


    modular.append_formated_modules(modules);
    modular.current_view = $.extend({}, modular.views[url_to_view_index]);
    modular.load_new_view();

})();

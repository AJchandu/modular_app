window.processes = new function () {
    var obj_this = this;
    this.log = {
        completed: [],
        running: []
    };


    this.add = function (process_name) {
        obj_this.log.running.push(process_name);
        setTimeout(function () {
            if (obj_this.log.running.length) {
                $(modular.containers.loading).css({
                    "diplay": "inline"
                });
                $(modular.containers.loading).stop().animate({
                    "opacity": "1"
                }, 300);
            }
        }, 300);

    };
    this.minus = function (process_name) {
        var idx = find_process_index(process_name);
        if (idx != "process not found") {
            obj_this.log.completed.push(obj_this.log.running.splice(idx, 1));
        }

        setTimeout(function () {
            if (!obj_this.log.running.length) {
                $(modular.containers.loading).stop().animate({
                    "opacity": "0"
                }, 300, function () {
                    $(modular.containers.loading).css({
                        "display": "none"
                    });
                });
            }
        }, 600);
    };

    function find_process_index(process_name) {
        for (i in obj_this.log.running) {
            if (obj_this.log.running[i] == process_name) {
                return i;
            }
        }
        return "process not found";
    }

};

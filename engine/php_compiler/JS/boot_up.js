$(document).on("click", "a", function (event) {
    var e = event ? event : window.event;
    if ($(this).attr("behaviour") == "default" || e.ctrlKey)
        return;

    var a_href = $(this).attr("href");
    var regex = new RegExp("^http");
    if (regex.test(a_href))
        return;

    e.preventDefault();

    if ($(this).attr("behaviour") == "none")
        return;

    var url = modular.url_pointer(a_href);
    modular.goto(url);
});

$(document).ready(function(){
    var script_path = $("script[name='combined']").attr("src");
    
    modular.project_path = script_path.replace("/app/combined.js","");
    modular.last_modified = "";
    
    //BEGIN create layout
    for (key in modular.modular_DOMs) {
        modular.modular_DOMs[key] = "#" + key + "_" + handy.randomizer();
    }

    var common_css = {
        "position": "absolute",
        "overflow": "hidden",
        "width": "100%",
        "height": "100%",
        "left": "0%",
        "top": "0%"
    };

    var index_css = {};
    if ("index" in modular.config)
        index_css = $.extend({}, common_css, modular.config.index);

    $("<div />").appendTo("body").attr({
        "id": modular.modular_DOMs.index.substr(1)
    }).css(index_css).append($("<div />").attr("id",modular.modular_DOMs.seperator.substr(1)).css("display","none"));

    $("<div />").appendTo("body").attr({
        "id": modular.modular_DOMs.url_error.substr(1)
    }).css({
        "opacity": "0"
    }).css(common_css);

    $("<div />").appendTo("body").attr({
        "id": modular.modular_DOMs.loading.substr(1)
    }).css({
        "background-color": "white",
        "transition": "background-color 2s"
    }).css(common_css);

    $("<div />").appendTo("body").attr({
        "id": modular.modular_DOMs.intro.substr(1)
    }).css({
        "background": "white"
    }).css(common_css);
    //**END create layout

    window.onpopstate = function (event) {
        modular.popstate(event);
    };

    //BEGIN formating modules entered in variables.js
    var modules = [];

    if ("initial_modules" in modular.config) {
        modules = modules.concat(modular.config.initial_modules);
    }

    console.log("\n\nInitial modules\n");
    console.log(modules);
    console.table(handy.clone_obj(modules));
    console.log("\n\n");

    modules = modular.check_clear_module_name_repetition(modules);

    modular.append_modules(modules);

});

<?php
require "vendor/autoload.php";
$variables = file_get_contents("../variables.json");
$config = file_get_contents("../app/config.json");
// $variables = json_decode($variables, true);
// $config = json_decode($config, true);
// $data = json_decode($variables);

// Validate
use Seld\JsonLint\JsonParser;
$parser = new JsonParser();
// returns null if it's valid json, or a ParsingException object.
//$report = $parser->lint($config);
$report = $parser->lint($variables);
echo $report->getMessage();
// echo $report->getDetails();
// Call getMessage() on the exception object to get
// a well formatted error message

// Call getDetails() on the exception to get more info.

// returns parsed json, like json_decode() does, but slower, throws
// exceptions on failure.
// $parser->parse($variables);
?>

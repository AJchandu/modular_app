var obj1="contact_us";
var obj2={module:"about_us",after:".price_container"};
var obj3=[
	{
		module:"about_us",
		after:".price_container"
	},
	"footer",
	{
		module:"contact_us",
		in:".contact_us_main_container"
	}
];


// var output=[
// 	{
// 		module:"about_us",
// 		in:"in_D",
// 		after:"after_D",
// 		state:"after_D",
// 	}
// ]

function module_format_parser(module_data){
	var module_list=[];
	function module_creater(obj){
		return {
			module: obj.module??"",
			in:obj.in??"in_D",
			after:obj.after??"after_D",
			state:obj.state??"state_D",
		};
	}
	if(typeof module_data == "string"){
		module_list.push(module_creater({
			"module" : module_data
		}));
	}else if(typeof module_data.length == "undefined"){
		module_list.push(module_creater(module_data));
	}else{
		for(var i=0;i<module_data.length;i++){
			var obj;
			if(typeof module_data[i] == "string"){
				obj = module_creater({
					"module" : module_data[i]
				});
			}else if(typeof module_data[i].length == "undefined"){
				obj = module_creater(module_data[i]);
			}
			module_list.push(obj);
		}
	}
	return module_list;
}

// var output =module_format_parser(obj1);
// var output =module_format_parser(obj2);
// var output =module_format_parser(obj3);
var output =module_format_parser([]);
console.log(output);
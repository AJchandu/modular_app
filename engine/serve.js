// require the module as normal
var fs = require("fs");
var browserSync = require("browser-sync").create();
const index = fs.readFileSync("../index.html");
browserSync.init({
  watch: true,
  server: "../",
  callbacks: {
      /**
       * This 'ready' callback can be used
       * to access the Browsersync instance
       */
      ready: function(err, bs) {

          // example of accessing URLS
          console.log(bs.options.get('urls'));

          // example of adding a middleware at the end
          // of the stack after Browsersync is running
          bs.addMiddleware("*", function (req, res) {
              // res.redirect('/');
              // res.writeHead(302, {
              //     location: "/"
              // }); 
              res.writeHead(302, {
                'Content-Length': Buffer.byteLength(index),
                'Content-Type': 'text/html'
              });
              res.end(index);
          });
      }
  }
});
browserSync.reload("../app/modules_processed/*");
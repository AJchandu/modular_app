import fs from 'fs';
import {config} from "../app/config.mjs";
import {
    modules_dir,
    modules_processed_dir,
    module_data_formatter,
    preprocess_and_save_module,
    combine_static_styles,
    combine_js
} from './formatter.mjs';

// import util from 'util';
// console.log(Object.keys(config));
// console.log(util.inspect(config["initial_modules"], {
//     showHidden: false,
//     depth: null
// }));

console.log('\n\x1b[1m\x1b[93m%s\x1b[0m', 'Converting all module declarations to OBJ format');


console.log("\x1b[1m%s\x1b[0m","main_modules");
for(let key in config["main_modules"]){
    config["main_modules"][key]["modules"]=module_data_formatter(config["main_modules"][key]["modules"],key);
}

console.log("\x1b[1m%s\x1b[0m","initial_modules");
config["initial_modules"]=module_data_formatter(config["initial_modules"],"initial_module");

console.log("\x1b[1m%s\x1b[0m","view actions");
for(var i=0;i<config["views"].length;i++){
    config["views"][i]["action"] = module_data_formatter(config["views"][i]["action"],"view");
}

if(!(fs.existsSync(modules_processed_dir))){
fs.mkdirSync(modules_processed_dir);
}

console.log("\n\x1b[1m\x1b[93m%s\x1b[0m","Substituting non existent modules");

// if(!array_key_exists("loading",$config)){
//     echo "Substituting loading module" . PHP_EOL;
//     $uid = uniqid();
//     $config["loading"]="loading_".$uid;
//     mkdir("../app/modules_processed/loading_".$uid);
//     system("cp substitutes/loading/* ../app/modules_processed/loading_".$uid);
//     rename("../app/modules_processed/loading_".$uid."/loading.html","../app/modules_processed/loading_".$uid."/loading_".$uid.".html");
//     rename("../app/modules_processed/loading_".$uid."/loading.less","../app/modules_processed/loading_".$uid."/loading_".$uid.".less");
// };
// if(!array_key_exists("url_error",$config)){
//     echo "Substituting url_error module" . PHP_EOL;
//     $uid = uniqid();
//     $config["url_error"]="url_error_".$uid;
//     mkdir("../app/modules_processed/url_error_".$uid);
//     system("cp substitutes/url_error/* ../app/modules_processed/url_error_".$uid);
//     rename("../app/modules_processed/url_error_".$uid."/url_error.html","../app/modules_processed/url_error_".$uid."/url_error_".$uid.".html");
//     rename("../app/modules_processed/url_error_".$uid."/url_error.less","../app/modules_processed/url_error_".$uid."/url_error_".$uid.".less");
// };

console.log("\n\x1b[1m\x1b[93m%s\x1b[0m","Processing all modules");

var modules_list = fs.readdirSync(modules_dir);
for(var idx=0;idx<modules_list.length;idx++) {
    // console.log(module_name);
    modules_list[idx] = {
        "module" : modules_list[idx],
        "units" : [] 
    };
    console.log("\x1b[1m%s\x1b[0m",modules_list[idx]["module"]);
    if (!(fs.existsSync(modules_processed_dir + modules_list[idx]["module"])))
        fs.mkdirSync(modules_processed_dir + modules_list[idx]["module"]);
    if(!preprocess_and_save_module(modules_list[idx])){
        break;
    }
};

combine_static_styles();
combine_js(modules_list);

export {modules_list};

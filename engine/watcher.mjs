// var chokidar = require('chokidar');
import {execSync} from 'child_process';
import chokidar from 'chokidar';
import {
  preprocess_and_save_module,
  combine_static_styles,
  combine_js
} from './formatter.mjs';

import {modules_list} from './parser.mjs';

var module_watcher = chokidar.watch('../app/modules', {
  ignored: /[\/\\]\./, persistent: true
});

// var log = console.log.bind(console);

module_watcher
  .on('add', function(path) { module_processor })
  .on('addDir', function(path) { module_processor })
  .on('change', module_processor)
  .on('unlink', function(path) {  module_processor })
  .on('unlinkDir', function(path) { module_processor })
  // .on('error', function(error) { log('Error happened', error); })
  // .on('ready', function() { log('Initial scan complete. Ready for changes.'); })
  // .on('raw', function(event, path, details) { log('Raw event info:', event, path, details); })

function module_processor(path){
   var dir = path.split('/');
   var module = dir[dir.length - 2];
   console.log("\n\x1b[1m\x1b[96m%s\x1b[0m","Reprocessing module :"+module);
   preprocess_and_save_module({"module" : module, "units" : []});
   
}

var res_watcher = chokidar.watch('../app/res', {
  ignored: /[\/\\]\./, persistent: true
});


res_watcher
  // .on('add', function(path) { module_processor })
  // .on('addDir', function(path) { log('Directory', path, 'has been added'); })
  .on('change', res_processor)
  // .on('unlink', function(path) { log('File', path, 'has been removed'); })
  // .on('unlinkDir', function(path) { log('Directory', path, 'has been removed'); })
  // .on('error', function(error) { log('Error happened', error); })
  // .on('ready', function() { log('Initial scan complete. Ready for changes.'); })
  // .on('raw', function(event, path, details) { log('Raw event info:', event, path, details); })

function res_processor(path){
  console.log("\n\x1b[1m\x1b[96m%s\x1b[0m","Reprocessing res directory");
  combine_static_styles();
  combine_js(modules_list);
}
console.log("\n\x1b[1m\x1b[92m%s\x1b[0m","Watcher is running...");
